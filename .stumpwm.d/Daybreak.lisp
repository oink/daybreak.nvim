;; ----- Colors -----
;;
;;
;;  #361f24  black   (background)
;;  #832f01  red
;;  #fb9756  orange
;;  #ffde6e  yellow
;;  #6b789b  blue
;;  #7c6a8e  purple
;;  #94b5ea  light blue 
;;  #fef9cd  white   (foreground)
;;
;;  #ffffff  white    (comment)
;;  #270e05  (current line)
;;  fffffff  (selection)
;;  #bd4307  (orange)

(set-fg-color "#fef9cd")
(set-bg-color "#361f24")

;;----- Boarder -----

;; Set focus and unfocus colors
(stumpwm:set-focus-color "#ffffff") 
(stumpwm:set-unfocus-color "#270e05")
(stumpwm:set-float-focus-color "#ffffff")
(stumpwm:set-float-unfocus-color "#270e05")

;;--- gaps ---

(load-module "swm-gaps")
(setf swm-gaps:*head-gaps-size*  0
      swm-gaps:*inner-gaps-size* 5
      swm-gaps:*outer-gaps-size* 40)
(when *initializing*
  (swm-gaps:toggle-gaps))

;;--- Font ---

(load-module "ttf-fonts")
(setq clx-truetype::*font-dirs*
      (append (list (namestring "~/.local/share/fonts"
              ))
          clx-truetype::*font-dirs*))
(set-font (make-instance 'xft:font
			 :family "DejaVuSansM Nerd Font"
                         :subfamily "Bold"
                         :size 11))

;;---------- Mode Line ----------

;; Run a shell command and format the output
(defun run-shell-command-and-format (command)
  (substitute #\Space #\Newline (run-shell-command command t)))

;; Show the kernel version
(defun show-kernel ()
  (run-shell-command-and-format "uname -r"))

;; Show the hostname
(defun show-hostname ()
  (run-shell-command-and-format "hostname"))

;; Show the window title
(defun show-window-title ()
  (substitute #\Space #\Newline (window-title (current-window))))

;; Mode Line Appearance
(setf stumpwm:*mode-line-background-color* "#270e05"
      stumpwm:*mode-line-foreground-color* "#fef9cd"
      stumpwm:*mode-line-border-color* "#ffffff"
      stumpwm:*mode-line-border-width* 0
      stumpwm:*mode-line-pad-x* 0
      stumpwm:*mode-line-pad-y* 0
      stumpwm:*mode-line-timeout* 5)

(when *initializing*
  (update-color-map (current-screen)))

;; Define the screen mode line format
(setf stumpwm:*screen-mode-line-format*
      (list "%g : %v ^>^7 : "
            '(:eval (show-hostname))
            ": " '(:eval (show-kernel)) 
            ": %d"))

;; Enable mode line
(dolist (X11-Head (screen-heads (current-screen)))
  (enable-mode-line (current-screen) X11-Head t))
