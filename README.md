# daybreak.nvim

A colorscheme for Neovim.

## Wallpaper
This is the wallpaper that the colorscheme is based on.

![Wallpaper.JPG](https://git.disroot.org/oink/daybreak.nvim/raw/branch/main/wallpaper.JPG)
