/* ... */

/* appearance */
static const unsigned int borderpx      = 1;        /* border pixel of windows */
static const unsigned int snap          = 32;       /* snap pixel */
static const unsigned int gappih        = 5;        /* horiz inner gap between windows */
static const unsigned int gappiv        = 5;        /* vert inner gap between windows */
static const unsigned int gappoh        = 40;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov        = 40;       /* vert outer gap between windows and screen edge */
static       int smartgaps              = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar                = 1;        /* 0 means no bar */
static const int topbar                 = 1;        /* 0 means bottom bar */
static const char *fonts[]              = { "DejaVuSansM Nerd Font:size=10:style=Bold" };
static const char dmenufont[]           = "DejaVuSansM Nerd Font:size=10:style=Bold";
static const char col_black_alt[]       = "#270e05";
static const char col_light_blue_alt[]  = "#97aee4";
static const char col_white[]           = "#fef9cd";
static const char col_white_alt[]       = "#ffffff";
static const char *colors[][3]          = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_black_alt, col_black_alt },
	[SchemeSel]  = { col_white, col_light_blue_alt, col_white_alt },
};

/* ... */

/* tagging */
static const char *tags[] = { "", "", "", "󰴱", "󰟵", "", "󱍚", "󰋅", "" };

/* ... */

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
/* ... */
static const int nstack      = 0;    /* number of clients in primary stack area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
/* ... */

static const Layout layouts[] = {
	/* symbol     arrange function, { nmaster, nstack, layout, master axis, stack axis, secondary stack axis } */
	{ "[]=",      flextile,         { -1, -1, -SPLIT_VERTICAL, TOP_TO_BOTTOM, TOP_TO_BOTTOM, 0, NULL } }, // default tile layout
	{ "><>",      NULL,             {0} },    /* no layout function means floating behavior */
	{ "[M]",      flextile,         { -1, -1, NO_SPLIT, MONOCLE, MONOCLE, 0, NULL } }, // monocle
	{ "|||",      flextile,         { -1, -1, SPLIT_VERTICAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, NULL } }, // columns (col) layout
	{ ">M>",      flextile,         { -1, -1, FLOATING_MASTER, LEFT_TO_RIGHT, LEFT_TO_RIGHT, 0, NULL } }, // floating master
	{ "[D]",      flextile,         { -1, -1, SPLIT_VERTICAL, TOP_TO_BOTTOM, MONOCLE, 0, NULL } }, // deck
	{ "TTT",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, LEFT_TO_RIGHT, 0, NULL } }, // bstack
	{ "===",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, NULL } }, // bstackhoriz
	{ "|M|",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, monoclesymbols } }, // centeredmaster
	{ ":::",      flextile,         { -1, -1, NO_SPLIT, GAPPLESSGRID, GAPPLESSGRID, 0, NULL } }, // gappless grid
	{ "[\\]",     flextile,         { -1, -1, NO_SPLIT, DWINDLE, DWINDLE, 0, NULL } }, // fibonacci dwindle
	{ "(@)",      flextile,         { -1, -1, NO_SPLIT, SPIRAL, SPIRAL, 0, NULL } }, // fibonacci spiral
	{ "[T]",      flextile,         { -1, -1, SPLIT_VERTICAL, LEFT_TO_RIGHT, TATAMI, 0, NULL } }, // tatami mats
};

/* ... */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black_alt, "-nf", col_white, "-sb", col_light_blue_alt, "-sf", col_white_alt, NULL };
/* ... */
