local o = vim.g.daybreak_color_overrides or {}

local colors = {
  red             = o.red             or { gui = '#832f01', cterm = '204',  cterm16 = '1'  },
  red_alt         = o.red_alt         or { gui = '#6b290f', cterm = '196',  cterm16 = '9'  }, 
  orange          = o.orange          or { gui = '#fb9756', cterm = '114',  cterm16 = '2'  }, 
  yellow          = o.yellow          or { gui = '#ffde6e', cterm = '180',  cterm16 = '3'  },
  yellow_alt      = o.yellow_alt      or { gui = '#ffba51', cterm = '173',  cterm16 = '11' }, 
  blue            = o.blue            or { gui = '#6b789b', cterm = '39',   cterm16 = '4'  }, 
  purple          = o.purple          or { gui = '#7c6a6e', cterm = '170',  cterm16 = '5'  },
  light_blue      = o.light_blue      or { gui = '#94b5ea', cterm = '38',   cterm16 = '6'  }, 
  white           = o.white           or { gui = '#fef9cd', cterm = '145',  cterm16 = '7'  },
  black           = o.black           or { gui = '#381f24', cterm = '235',  cterm16 = '0'  },
  visual_white    = o.visual_white    or { gui = 'NONE',    cterm = 'NONE', cterm16 = '0'  },
  comment_white   = o.comment_white   or { gui = '#ffffff', cterm = '59',   cterm16 = '15' }, 
  gutter_fg_white = o.gutter_fg_white or { gui = '#ffffff', cterm = '238',  cterm16 = '15' }, 
  cursor_black    = o.cursor_black    or { gui = '#270e05', cterm = '236',  cterm16 = '8'  }, 
  visual_white    = o.visual_white    or { gui = '#ffffff', cterm = '237',  cterm16 = '15' },  
  menu_white      = o.menu_white      or { gui = '#270e05', cterm = '237',  cterm16 = '8'  },
  special_white   = o.special_white   or { gui = '#ffffff', cterm = '238',  cterm16 = '15' },  
  vertsplit       = o.vertsplit       or { gui = '#ffffff', cterm = '59',   cterm16 = '15' },
}

if (vim.g.daybreak_termcolors or 256) == 16 then
  for _, color in pairs(colors) do
    color.cterm = color.cterm16
    color.cterm16 = nil
  end
end

return colors
