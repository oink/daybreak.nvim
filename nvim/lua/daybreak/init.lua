local colors = require 'daybreak.colors'

vim.cmd('highlight clear')

if vim.g.syntax_on ~= nil then
  vim.cmd('syntax reset')
end

vim.cmd('set t_Co=256')

vim.g.colors_name = 'daybreak'

local terminal_italics = vim.g.daybreak_terminal_italics or false

local red = colors.red
local red_alt = colors.red_alt
local orange = colors.orange
local yellow = colors.yellow
local yellow_alt = colors.yellow_alt
local blue = colors.blue
local purple = colors.purple
local light_blue = colors.light_blue
local white = colors.white
local black = colors.black
local visual_white = colors.visual_white
local comment_white = colors.comment_white
local gutter_fg_white = colors.gutter_fg_white
local cursor_black = colors.cursor_black
local visual_white = colors.visual_white
local menu_white = colors.menu_white
local special_white = colors.special_white
local vertsplit = colors.vertsplit

vim.g.terminal_ansi_colors = {
  black.gui, red.gui, orange.gui, yellow.gui,
  blue.gui, purple.gui, light_blue.gui, white.gui,
  visual_white.gui, red_alt.gui, orange.gui, yellow_alt.gui,
  blue.gui, purple.gui, light_blue.gui, comment_white.gui,
}

local function h(group, style)
  if not terminal_italics then
    if style.cterm == 'italic' then
      style.cterm = nil
    end
    if style.gui == 'italic' then
      style.gui = nil
    end
  end

  local guifg, guibg, guisp, ctermfg, ctermbg

  if style.fg then
    guifg = style.fg.gui
    ctermfg = style.fg.cterm
  else
    guifg = 'NONE'
    ctermfg = 'NONE'
  end

  if style.bg then
    guibg = style.bg.gui
    ctermbg = style.bg.cterm
  else
    guibg = 'NONE'
    ctermbg = 'NONE'
  end

  if style.sp then
    guisp = style.sp.gui
  else
    guisp = 'NONE'
  end

  local gui = style.gui or 'NONE'
  local cterm = style.cterm or 'NONE'

  vim.cmd('highlight ' .. group
    .. ' guifg='   .. guifg
    .. ' guibg='   .. guibg
    .. ' guisp='   .. guisp
    .. ' gui='     .. gui
    .. ' ctermfg=' .. ctermfg
    .. ' ctermbg=' .. ctermbg
    .. ' cterm='   .. cterm
  )
end

h('Comment',        { fg = comment_white, gui = 'italic', cterm = 'italic' })
h('Constant',       { fg = light_blue })
h('String',         { fg = orange })
h('Character',      { fg = orange })
h('Number',         { fg = yellow_alt })
h('Boolean',        { fg = yellow_alt })
h('Float',          { fg = yellow_alt })
h('Identifier',     { fg = red })
h('Function',       { fg = blue })
h('Statement',      { fg = purple })
h('Conditional',    { fg = purple })
h('Repeat',         { fg = purple })
h('Label',          { fg = purple })
h('Operator',       { fg = purple })
h('Keyword',        { fg = red })
h('Exception',      { fg = purple })
h('PreProc',        { fg = yellow })
h('Include',        { fg = blue })
h('Define',         { fg = purple })
h('Macro',          { fg = purple })
h('PreCondit',      { fg = yellow })
h('Type',           { fg = yellow })
h('StorageClass',   { fg = yellow })
h('Structure',      { fg = yellow })
h('Typedef',        { fg = yellow })
h('Special',        { fg = blue })
h('SpecialChar',    { fg = yellow_alt })
h('Tag',            { })
h('Delimiter',      { })
h('SpecialComment', { fg = comment_white })
h('Debug',          { })
h('Underlined',     { gui = 'underline', cterm = 'underline' })
h('Ignore',         { })
h('Error',          { fg = red })
h('Todo',           { fg = purple })

h('ColorColumn',      { bg = cursor_black })
h('Conceal',          { })
h('Cursor',           { fg = black, bg = blue })
h('CursorIM',         { })
h('CursorColumn',     { bg = cursor_black })
if vim.wo.diff then
  h('CursorLine',     { gui = 'underline' })
else
  h('CursorLine',     { bg = cursor_black })
end
h('Directory',        { fg = blue })
h('DiffAdd',          { bg = orange, fg = black })
h('DiffChange',       { fg = yellow, gui = 'underline', cterm = 'underline' })
h('DiffDelete',       { bg = red, fg = black })
h('DiffText',         { bg = yellow, fg = black })
if vim.g.daybreak_hide_endofbuffer or false then
  h('EndOfBuffer',    { fg = black })
end
h('ErrorMsg',         { fg = red })
h('VertSplit',        { fg = vertsplit })
h('Folded',           { fg = comment_white })
h('FoldColumn',       { })
h('SignColumn',       { })
h('IncSearch',        { fg = yellow, bg = comment_white })
h('LineNr',           { fg = gutter_fg_white })
h('CursorLineNr',     { })
h('MatchParen',       { fg = blue, gui = 'underline', cterm = 'underline' })
h('ModeMsg',          { })
h('MoreMsg',          { })
h('NonText',          { fg = special_white })
h('Normal',           { fg = white, bg = black })
h('Pmenu',            { bg = menu_white })
h('PmenuSel',         { fg = black, bg = blue })
h('PmenuSbar',        { bg = special_white })
h('PmenuThumb',       { bg = white })
h('Question',         { fg = purple })
h('QuickFixLine',     { fg = black, bg = yellow })
h('Search',           { fg = black, bg = yellow })
h('SpecialKey',       { fg = special_white })
h('SpellBad',         { fg = red, gui = 'underline', cterm = 'underline' })
h('SpellCap',         { fg = yellow_alt })
h('SpellLocal',       { fg = yellow_alt })
h('SpellRare',        { fg = yellow_alt })
h('StatusLine',       { fg = white, bg = cursor_black })
h('StatusLineNC',     { fg = comment_white })
h('StatusLineTerm',   { fg = white, bg = cursor_black })
h('StatusLineTermNC', { fg = comment_white })
h('TabLine',          { fg = comment_white })
h('TabLineFill',      { })
h('TabLineSel',       { fg = white })
h('Terminal',         { fg = white, bg = black })
h('Title',            { fg = orange })
h('Visual',           { fg = visual_white, bg = visual_white })
h('VisualNOS',        { bg = visual_white })
h('WarningMsg',       { fg = yellow })
h('WildMenu',         { fg = black, bg = blue })

-- Termdebug
h('debugPC',          { bg = special_white })
h('debugBreakpoint',  { fg = black, bg = red })

-- CSS
h('cssAttrComma',         { fg = purple })
h('cssAttributeSelector', { fg = orange })
h('cssBraces',            { fg = white })
h('cssClassName',         { fg = yellow_alt })
h('cssClassNameDot',      { fg = yellow_alt })
h('cssDefinition',        { fg = purple })
h('cssFontAttr',          { fg = yellow_alt })
h('cssFontDescriptor',    { fg = purple })
h('cssFunctionName',      { fg = blue })
h('cssIdentifier',        { fg = blue })
h('cssImportant',         { fg = purple })
h('cssInclude',           { fg = white })
h('cssIncludeKeyword',    { fg = purple })
h('cssMediaType',         { fg = yellow_alt })
h('cssProp',              { fg = white })
h('cssPseudoClassId',     { fg = yellow_alt })
h('cssSelectorOp',        { fg = purple })
h('cssSelectorOp2',       { fg = purple })
h('cssTagName',           { fg = red })

-- Fish Shell
h('fishKeyword',     { fg = purple })
h('fishConditional', { fg = purple })

-- Go
h('goDeclaration',  { fg = purple })
h('goBuiltins',     { fg = light_blue })
h('goFunctionCall', { fg = blue })
h('goVarDefs',      { fg = red })
h('goVarAssign',    { fg = red })
h('goVar',          { fg = purple })
h('goConst',        { fg = purple })
h('goType',         { fg = yellow })
h('goTypeName',     { fg = yellow })
h('goDeclType',     { fg = light_blue })
h('goTypeDecl',     { fg = purple })

-- HTML (keep consistent with Markdown, below)
h('htmlArg',            { fg = yellow_alt })
h('htmlBold',           { fg = yellow_alt, gui = 'bold', cterm = 'bold' })
h('htmlEndTag',         { fg = white })
h('htmlH1',             { fg = red })
h('htmlH2',             { fg = red })
h('htmlH3',             { fg = red })
h('htmlH4',             { fg = red })
h('htmlH5',             { fg = red })
h('htmlH6',             { fg = red })
h('htmlItalic',         { fg = purple, gui = 'italic', cterm = 'italic' })
h('htmlLink',           { fg = light_blue, gui = 'underline', cterm = 'underline' })
h('htmlSpecialChar',    { fg = yellow_alt })
h('htmlSpecialTagName', { fg = red })
h('htmlTag',            { fg = white })
h('htmlTagN',           { fg = red })
h('htmlTagName',        { fg = red })
h('htmlTitle',          { fg = white })

-- JavaScript
h('javaScriptBraces',       { fg = white })
h('javaScriptFunction',     { fg = purple })
h('javaScriptIdentifier',   { fg = purple })
h('javaScriptNull',         { fg = yellow_alt })
h('javaScriptNumber',       { fg = yellow_alt })
h('javaScriptRequire',      { fg = light_blue })
h('javaScriptReserved',     { fg = purple })
-- http//github.com/pangloss/vim-javascript
h('jsArrowFunction',        { fg = purple })
h('jsClassKeyword',         { fg = purple })
h('jsClassMethodType',      { fg = purple })
h('jsDocParam',             { fg = blue })
h('jsDocTags',              { fg = purple })
h('jsExport',               { fg = purple })
h('jsExportDefault',        { fg = purple })
h('jsExtendsKeyword',       { fg = purple })
h('jsFrom',                 { fg = purple })
h('jsFuncCall',             { fg = blue })
h('jsFunction',             { fg = purple })
h('jsGenerator',            { fg = yellow })
h('jsGlobalObjects',        { fg = yellow })
h('jsImport',               { fg = purple })
h('jsModuleAs',             { fg = purple })
h('jsModuleWords',          { fg = purple })
h('jsModules',              { fg = purple })
h('jsNull',                 { fg = yellow_alt })
h('jsOperator',             { fg = purple })
h('jsStorageClass',         { fg = purple })
h('jsSuper',                { fg = red })
h('jsTemplateBraces',       { fg = red_alt })
h('jsTemplateVar',          { fg = orange })
h('jsThis',                 { fg = red })
h('jsUndefined',            { fg = yellow_alt })
-- http//github.com/othree/yajs.vim
h('javascriptArrowFunc',    { fg = purple })
h('javascriptClassExtends', { fg = purple })
h('javascriptClassKeyword', { fg = purple })
h('javascriptDocNotation',  { fg = purple })
h('javascriptDocParamName', { fg = blue })
h('javascriptDocTags',      { fg = purple })
h('javascriptEndColons',    { fg = white })
h('javascriptExport',       { fg = purple })
h('javascriptFuncArg',      { fg = white })
h('javascriptFuncKeyword',  { fg = purple })
h('javascriptIdentifier',   { fg = red })
h('javascriptImport',       { fg = purple })
h('javascriptMethodName',   { fg = white })
h('javascriptObjectLabel',  { fg = white })
h('javascriptOpSymbol',     { fg = light_blue })
h('javascriptOpSymbols',    { fg = light_blue })
h('javascriptPropertyName', { fg = orange })
h('javascriptTemplateSB',   { fg = red_alt })
h('javascriptVariable',     { fg = purple })

-- JSON
h('jsonCommentError',      { fg = white })
h('jsonKeyword',           { fg = red })
h('jsonBoolean',           { fg = yellow_alt })
h('jsonNumber',            { fg = yellow_alt })
h('jsonQuote',             { fg = white })
h('jsonMissingCommaError', { fg = red, gui = 'reverse' })
h('jsonNoQuotesError',     { fg = red, gui = 'reverse' })
h('jsonNumError',          { fg = red, gui = 'reverse' })
h('jsonString',            { fg = orange })
h('jsonStringSQError',     { fg = red, gui = 'reverse' })
h('jsonSemicolonError',    { fg = red, gui = 'reverse' })

-- LESS
h('lessVariable',      { fg = purple })
h('lessAmpersandChar', { fg = white })
h('lessClass',         { fg = yellow_alt })

-- Markdown (keep consistent with HTML, above)
h('markdownBlockquote',        { fg = comment_white })
h('markdownBold',              { fg = yellow_alt, gui = 'bold', cterm = 'bold' })
h('markdownCode',              { fg = orange })
h('markdownCodeBlock',         { fg = orange })
h('markdownCodeDelimiter',     { fg = orange })
h('markdownH1',                { fg = red })
h('markdownH2',                { fg = red })
h('markdownH3',                { fg = red })
h('markdownH4',                { fg = red })
h('markdownH5',                { fg = red })
h('markdownH6',                { fg = red })
h('markdownHeadingDelimiter',  { fg = red })
h('markdownHeadingRule',       { fg = comment_white })
h('markdownId',                { fg = purple })
h('markdownIdDeclaration',     { fg = blue })
h('markdownIdDelimiter',       { fg = purple })
h('markdownItalic',            { fg = purple, gui = 'italic', cterm = 'italic' })
h('markdownLinkDelimiter',     { fg = purple })
h('markdownLinkText',          { fg = blue })
h('markdownListMarker',        { fg = red })
h('markdownOrderedListMarker', { fg = red })
h('markdownRule',              { fg = comment_white })
h('markdownUrl',               { fg = light_blue, gui = 'underline', cterm = 'underline' })

-- Perl
h('perlFiledescRead',      { fg = orange })
h('perlFunction',          { fg = purple })
h('perlMatchStartEnd',     { fg = blue })
h('perlMethod',            { fg = purple })
h('perlPOD',               { fg = comment_white })
h('perlSharpBang',         { fg = comment_white })
h('perlSpecialString',     { fg = yellow_alt })
h('perlStatementFiledesc', { fg = red })
h('perlStatementFlow',     { fg = red })
h('perlStatementInclude',  { fg = purple })
h('perlStatementScalar',   { fg = purple })
h('perlStatementStorage',  { fg = purple })
h('perlSubName',           { fg = yellow })
h('perlVarPlain',          { fg = blue })

-- PHP
h('phpVarSelector',    { fg = red })
h('phpOperator',       { fg = white })
h('phpParent',         { fg = white })
h('phpMemberSelector', { fg = white })
h('phpType',           { fg = purple })
h('phpKeyword',        { fg = purple })
h('phpClass',          { fg = yellow })
h('phpUseClass',       { fg = white })
h('phpUseAlias',       { fg = white })
h('phpInclude',        { fg = purple })
h('phpClassExtends',   { fg = orange })
h('phpDocTags',        { fg = white })
h('phpFunction',       { fg = blue })
h('phpFunctions',      { fg = light_blue })
h('phpMethodsVar',     { fg = yellow_alt })
h('phpMagicConstants', { fg = yellow_alt })
h('phpSuperglobals',   { fg = red })
h('phpConstants',      { fg = yellow_alt })

-- Ruby
h('rubyBlockParameter',            { fg = red})
h('rubyBlockParameterList',        { fg = red })
h('rubyClass',                     { fg = purple})
h('rubyConstant',                  { fg = yellow})
h('rubyControl',                   { fg = purple })
h('rubyEscape',                    { fg = red})
h('rubyFunction',                  { fg = blue})
h('rubyGlobalVariable',            { fg = red})
h('rubyInclude',                   { fg = blue})
h('rubyIncluderubyGlobalVariable', { fg = red})
h('rubyInstanceVariable',          { fg = red})
h('rubyInterpolation',             { fg = light_blue })
h('rubyInterpolationDelimiter',    { fg = red })
h('rubyInterpolationDelimiter',    { fg = red})
h('rubyRegexp',                    { fg = light_blue})
h('rubyRegexpDelimiter',           { fg = light_blue})
h('rubyStringDelimiter',           { fg = orange})
h('rubySymbol',                    { fg = light_blue})

-- Sass
-- http//github.com/tpope/vim-haml
h('sassAmpersand',      { fg = red })
h('sassClass',          { fg = yellow_alt })
h('sassControl',        { fg = purple })
h('sassExtend',         { fg = purple })
h('sassFor',            { fg = white })
h('sassFunction',       { fg = light_blue })
h('sassId',             { fg = blue })
h('sassInclude',        { fg = purple })
h('sassMedia',          { fg = purple })
h('sassMediaOperators', { fg = white })
h('sassMixin',          { fg = purple })
h('sassMixinName',      { fg = blue })
h('sassMixing',         { fg = purple })
h('sassVariable',       { fg = purple })
-- http//github.com/cakebaker/scss-syntax.vim
h('scssExtend',         { fg = purple })
h('scssImport',         { fg = purple })
h('scssInclude',        { fg = purple })
h('scssMixin',          { fg = purple })
h('scssSelectorName',   { fg = yellow_alt })
h('scssVariable',       { fg = purple })

-- TeX
h('texStatement',    { fg = purple })
h('texSubscripts',   { fg = yellow_alt })
h('texSuperscripts', { fg = yellow_alt })
h('texTodo',         { fg = red_alt })
h('texBeginEnd',     { fg = purple })
h('texBeginEndName', { fg = blue })
h('texMathMatcher',  { fg = blue })
h('texMathDelim',    { fg = blue })
h('texDelimiter',    { fg = yellow_alt })
h('texSpecialChar',  { fg = yellow_alt })
h('texCite',         { fg = blue })
h('texRefZone',      { fg = blue })

-- TypeScript
h('typescriptReserved',  { fg = purple })
h('typescriptEndColons', { fg = white })
h('typescriptBraces',    { fg = white })

-- XML
h('xmlAttrib',  { fg = yellow_alt })
h('xmlEndTag',  { fg = red })
h('xmlTag',     { fg = red })
h('xmlTagName', { fg = red })

-- airblade/vim-gitgutter
h("GitGutterAdd",    { fg = orange })
h("GitGutterChange", { fg = yellow })
h("GitGutterDelete", { fg = red })

-- dense-analysis/ale
h('ALEError',   { fg = red, gui = 'underline', cterm = 'underline' })
h('ALEWarning', { fg = yellow, gui = 'underline', cterm = 'underline'})
h('ALEInfo',    { gui = 'underline', cterm = 'underline'})

-- easymotion/vim-easymotion
h('EasyMotionTarget',        { fg = red, gui = 'bold', cterm = 'bold' })
h('EasyMotionTarget2First',  { fg = yellow, gui = 'bold', cterm = 'bold' })
h('EasyMotionTarget2Second', { fg = yellow_alt, gui = 'bold', cterm = 'bold' })
h('EasyMotionShade',         { fg = comment_white })

-- lewis6991/gitsigns.nvim
vim.cmd('hi link GitSignsAdd    GitGutterAdd')
vim.cmd('hi link GitSignsChange GitGutterChange')
vim.cmd('hi link GitSignsDelete GitGutterDelete')

-- mhinz/vim-signify
vim.cmd('hi link SignifySignAdd    GitGutterAdd')
vim.cmd('hi link SignifySignChange GitGutterChange')
vim.cmd('hi link SignifySignDelete GitGutterDelete')

-- neomake/neomake
h('NeomakeWarningSign', { fg = yellow })
h('NeomakeErrorSign',   { fg = red })
h('NeomakeInfoSign',    { fg = blue })

-- plasticboy/vim-markdown (keep consistent with Markdown, above)
h('mkdDelimiter', { fg = purple })
h('mkdHeading',   { fg = red })
h('mkdLink',      { fg = blue })
h('mkdURL',       { fg = light_blue, gui = 'underline', cterm = 'underline' })

-- prabirshrestha/vim-lsp
h("LspError",       { fg = red })
h("LspWarning",     { fg = yellow })
h("LspInformation", { fg = blue })
h("LspHint",        { fg = light_blue })

-- tpope/vim-fugitive
h('diffAdded',   { fg = orange })
h('diffRemoved', { fg = red })

-- Git Highlighting
h('gitcommitComment',       { fg = comment_white })
h('gitcommitUnmerged',      { fg = orange })
h('gitcommitOnBranch',      { })
h('gitcommitBranch',        { fg = purple })
h('gitcommitDiscardedType', { fg = red })
h('gitcommitSelectedType',  { fg = orange })
h('gitcommitHeader',        { })
h('gitcommitUntrackedFile', { fg = light_blue })
h('gitcommitDiscardedFile', { fg = red })
h('gitcommitSelectedFile',  { fg = orange })
h('gitcommitUnmergedFile',  { fg = yellow })
h('gitcommitFile',          { })
h('gitcommitSummary',       { fg = white })
h('gitcommitOverflow',      { fg = red })
vim.cmd('hi link gitcommitNoBranch gitcommitBranch')
vim.cmd('hi link gitcommitUntracked gitcommitComment')
vim.cmd('hi link gitcommitDiscarded gitcommitComment')
vim.cmd('hi link gitcommitSelected gitcommitComment')
vim.cmd('hi link gitcommitDiscardedArrow gitcommitDiscardedFile')
vim.cmd('hi link gitcommitSelectedArrow gitcommitSelectedFile')
vim.cmd('hi link gitcommitUnmergedArrow gitcommitUnmergedFile')

-- Neovim terminal colors
vim.g.terminal_color_0  = black.gui
vim.g.terminal_color_1  = red.gui
vim.g.terminal_color_2  = orange.gui
vim.g.terminal_color_3  = yellow.gui
vim.g.terminal_color_4  = blue.gui
vim.g.terminal_color_5  = purple.gui
vim.g.terminal_color_6  = light_blue.gui
vim.g.terminal_color_7  = white.gui
vim.g.terminal_color_8  = visual_white.gui
vim.g.terminal_color_9  = red_alt.gui
vim.g.terminal_color_10 = orange.gui
vim.g.terminal_color_11 = yellow_alt.gui
vim.g.terminal_color_12 = blue.gui
vim.g.terminal_color_13 = purple.gui
vim.g.terminal_color_14 = light_blue.gui
vim.g.terminal_color_15 = comment_white.gui
vim.g.terminal_color_background = vim.g.terminal_color_0
vim.g.terminal_color_foreground = vim.g.terminal_color_7

-- Neovim LSP colors
if vim.diagnostic then
  h('DiagnosticError',            { fg = red })
  h('DiagnosticWarn',             { fg = yellow })
  h('DiagnosticInfo',             { fg = blue })
  h('DiagnosticHint',             { fg = light_blue })
  h('DiagnosticUnderlineError',   { fg = red, gui = 'underline', cterm = 'underline' })
  h('DiagnosticUnderlineWarn',    { fg = yellow, gui = 'underline', cterm = 'underline' })
  h('DiagnosticUnderlineInfo',    { fg = blue, gui = 'underline', cterm = 'underline' })
  h('DiagnosticUnderlineHint',    { fg = light_blue, gui = 'underline', cterm = 'underline' })
  h('DiagnosticSignError',        { fg = red })
  h('DiagnosticSignWarn',         { fg = yellow })
  h('DiagnosticSignInfo',         { fg = blue })
  h('DiagnosticSignHint',         { fg = light_blue })
  h('DiagnosticVirtualTextError', { fg = red, bg = cursor_black })
  h('DiagnosticVirtualTextWarn',  { fg = yellow, bg = cursor_black })
  h('DiagnosticVirtualTextInfo',  { fg = blue, bg = cursor_black })
  h('DiagnosticVirtualTextHint',  { fg = light_blue, bg = cursor_black })
else
  h('LspDiagnosticsDefaultError',         { fg = red })
  h('LspDiagnosticsDefaultWarning',       { fg = yellow })
  h('LspDiagnosticsDefaultInformation',   { fg = blue })
  h('LspDiagnosticsDefaultHint',          { fg = light_blue })
  h('LspDiagnosticsUnderlineError',       { fg = red, gui = 'underline', cterm = 'underline' })
  h('LspDiagnosticsUnderlineWarning',     { fg = yellow, gui = 'underline', cterm = 'underline' })
  h('LspDiagnosticsUnderlineInformation', { fg = blue, gui = 'underline', cterm = 'underline' })
  h('LspDiagnosticsUnderlineHint',        { fg = light_blue, gui = 'underline', cterm = 'underline' })
  h('LspDiagnosticsSignError',            { fg = red })
  h('LspDiagnosticsSignWarning',          { fg = yellow })
  h('LspDiagnosticsSignInformation',      { fg = blue })
  h('LspDiagnosticsSignHint',             { fg = light_blue })
  h('LspDiagnosticsVirtualTextError',     { fg = red, bg = cursor_black })
  h('LspDiagnosticsVirtualTextWarning',   { fg = yellow, bg = cursor_black })
  h('LspDiagnosticsVirtualTextInformation', { fg = blue, bg = cursor_black })
  h('LspDiagnosticsVirtualTextHint',      { fg = light_blue, bg = cursor_black })
end

vim.o.background = 'dark'
