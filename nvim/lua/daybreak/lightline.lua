local colors = require 'daybreak.colors'

local red = { colors.red.gui, colors.red.cterm }
local orange = { colors.orange.gui, colors.orange.cterm }
local yellow = { colors.yellow.gui, colors.yellow.cterm }
local blue = { colors.blue.gui, colors.blue.cterm }
local purple = { colors.purple.gui, colors.purple.cterm }
local white = { colors.white.gui, colors.white.cterm }
local cursor_black = { colors.cursor_black.gui, colors.cursor_black.cterm }
local visual_white = { colors.visual_white.gui, colors.visual_white.cterm }

local p = {
  normal = {
    left = { { cursor_black, orange }, { white, visual_white } },
    right = { { cursor_black, orange }, { white, visual_white } },
    middle = { { white, cursor_black } },
    error = { { cursor_black, red } },
    warning = { { cursor_black, yellow } },
  },
  inactive = {
    left =  { { white, visual_white }, { white, visual_white } },
    right = { { cursor_black, white }, { cursor_black, white } },
    middle = { { white, visual_white } },
  },
  insert = {
    left = { { cursor_black, blue }, { white, visual_white } },
    right = { { cursor_black, blue }, { white, visual_white } },
  },
  replace = {
    left = { { cursor_black, red }, { white, visual_white } },
    right = { { cursor_black, red }, { white, visual_white } },
  },
  visual = {
    left = { { cursor_black, purple }, { white, visual_white } },
    right = { { cursor_black, purple }, { white, visual_white } },
  },
  tabline = {
    left = { { white, visual_white } },
    tabsel = { { cursor_black, white } },
    middle = { { white, cursor_black } },
    right = { { white, visual_white } },
  },
}

vim.g['lightline#colorscheme#daybreak#palette'] = vim.fn['lightline#colorscheme#flatten'](p)
