/* ... */

static char *font = "DejaVuSansM Nerd Font:pixelsize=12:antialias=true:autohint=true";
static int borderpx = 20;

/* ... */

/*
 * thickness of underline and bar cursors
 */
static unsigned int cursorthickness = 2;

/* ... */

/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#361f24",
	"#832f01",
	"#fb9756",
	"#ffde6e",
	"#6b789b",
	"#7c6a8e",
	"#94b5ea",
	"#fef9cd",

	/* 8 bright colors */
	"#270e05",
	"#6b290f",
	"#bd4307",
	"#ffba51",
	"#5376ae",
	"#654d86",
	"#97aee4",
	"#ffffff",

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#ffffff",
	"#361f24",
	"#fef9cd", /* default foreground colour */
	"#361f24", /* default background colour */
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;

/*
 * Default shape of cursor
 * 2: Block ("█")
 * 4: Underline ("_")
 * 6: Bar ("|")
 * 7: Snowman ("☃")
 */
static unsigned int cursorshape = 2;

/*
 * Default columns and rows numbers
 */

static unsigned int cols = 80;
static unsigned int rows = 24;

/*
 * Default colour and shape of the mouse cursor
 */
static unsigned int mouseshape = XC_xterm;
static unsigned int mousefg = 7;
static unsigned int mousebg = 0;

/*
 * Color used to display font attributes when fontconfig selected a font which
 * doesn't match the ones requested.
 */
static unsigned int defaultattr = 11;

/* ... */
