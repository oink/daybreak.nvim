# Prompt #
autoload -U colors && colors

# No Directory Shortening
#PS1="%{$fg[cyan]%}%n%{$reset_color%}@%{$fg[red]%}%m %{$fg[magenta]%}%~ %{$reset_color%}%% "

# Directory Shortening
PS1="%{$fg[cyan]%}%n%{$reset_color%}@%{$fg[red]%}%m %{$fg[magenta]%}%(5~|%-1~/.../%3~|%4~) %{$reset_color%}%% " 
